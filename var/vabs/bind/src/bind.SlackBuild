#!/bin/sh

# Copyright 2006, 2007, 2008, 2009, 2010, 2011, 2012  Patrick J. Volkerding, Sebeka, MN, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


NAME="bind"
VERSION=${VERSION:-"9.9.3-P2"}
BUILDNUM=${BUILDNUM:-"1"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
LINK=${LINK:-"ftp://ftp.isc.org/isc/bind9/${VERSION}/${NAME}-${VERSION}.tar.gz"}
ARCH=${ARCH:-"$(uname -m)"}
MAKEDEPENDS=${MAKEDEPENDS:-""}

# Automatically determine the architecture we're building on:
if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) export ARCH=i586 ;;
    arm*) export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
       *) export ARCH=$( uname -m ) ;;
  esac
fi

NUMJOBS=${NUMJOBS:-" -j7 "}
PKGNAM=${NAME}
CWD=$(pwd)
TMP=/bind-$(mcookie)
PKG=$TMP/package-${PKGNAM}
RELEASEDIR=${RELEASEDIR:-"${CWD}/.."}

rm -rf $PKG
mkdir -p $TMP $PKG/etc/default

if [ "$ARCH" = "i?86" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
  CONFIGURE_TRIPLET="i586-vector-linux"
elif [ "$ARCH" = "s390" ]; then
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
  CONFIGURE_TRIPLET="x86_64-vlocity-linux"
fi

if [ "$NORUN" != 1 ]; then


# Download the source code
for src in $(echo $LINK); do
	(
	cd $CWD
	wget -c --no-check-certificate $src
	)
done
cd $TMP
rm -rf ${PKGNAM}-${VERSION}
tar xvf $CWD/${PKGNAM}-$VERSION.tar.gz || exit 1
cd ${PKGNAM}-$VERSION || exit 1

# Remove use of SO_BSDCOMPAT which has been obsolete since the 2.2.x kernel
# series, and generates warnings under 2.6.x kernels.  This _might_ be fixed
# upstream already, but an explicit #undef SO_BSDCOMPAT does not hurt:
zcat $CWD/bind.so_bsdcompat.diff.gz | patch -p1 --verbose || exit

# Make sure ownerships and permissions are sane:
chown -R root:root .
find . \
 \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \;

# Configure:
CFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --with-libtool \
  --mandir=/usr/man \
  --enable-shared \
  --disable-static \
  --enable-threads \
  --with-openssl=/usr \
  --build=$CONFIGURE_TRIPLET || exit 1

# Build and install:
make $NUMJOBS || exit 1
make install DESTDIR=$PKG || exit 1

# We like symlinks.
( cd $PKG/usr/sbin
  ln -sf named lwresd
)

# We like a lot of symlinks.
( cd $PKG/usr/man/man3
  sh $CWD/3link.sh
)

# Install init script:
mkdir -p $PKG/etc/rc.d
cp -a $CWD/rc.bind $PKG/etc/rc.d/rc.bind.new
chmod 644 $PKG/etc/rc.d/rc.bind.new

# Add /var/run/named directory:
mkdir -p $PKG/var/run/named

# Fix library perms:
chmod 755 $PKG/usr/lib${LIBDIRSUFFIX}/*

# Strip binaries:
find $PKG | xargs file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null

# Compress and link manpages, if any:
if [ -d $PKG/usr/man ]; then
  ( cd $PKG/usr/man
    for manpagedir in $(find . -type d -name "man*") ; do
      ( cd $manpagedir
        for eachpage in $( find . -type l -maxdepth 1) ; do
          ln -s $( readlink $eachpage ).gz $eachpage.gz
          rm $eachpage
        done
        gzip -9 *.* 2> /dev/null
      )
    done
  )
fi

# Add a documentation directory:
mkdir -p $PKG/usr/doc/${PKGNAM}-$VERSION
cp -a \
  CHANGES COPYRIGHT FAQ* README* \
  doc/arm doc/misc \
  $PKG/usr/doc/${PKGNAM}-$VERSION 

# This one should have the correct perms of the config file:
chmod 644 $PKG/usr/doc/${PKGNAM}-$VERSION/misc/rndc.conf-sample

# One format of this is plenty.  Especially get rid of the bloated PDF.
( cd $PKG/usr/doc/bind-$VERSION/arm
  rm -f Makefile* *.pdf *.xml README.SGML latex-fixup.pl
)

# Add sample config files for a simple caching nameserver:
mkdir -p $PKG/var/named/caching-example
cat $CWD/caching-example/named.conf > $PKG/etc/named.conf.new
cat $CWD/caching-example/localhost.zone > $PKG/var/named/caching-example/localhost.zone
cat $CWD/caching-example/named.local > $PKG/var/named/caching-example/named.local
cat $CWD/caching-example/named.root > $PKG/var/named/caching-example/named.root
# This name is deprecated, but having it here doesn't hurt in case
# an old configuration file wants it:
cat $CWD/caching-example/named.root > $PKG/var/named/caching-example/named.ca

mkdir -p $PKG/install
zcat $CWD/doinst.sh.gz > $PKG/install/doinst.sh
cat $CWD/slack-desc > $PKG/install/slack-desc
cat $CWD/slack-desc > $RELEASEDIR/slack-desc

cd $PKG

echo "Finding dependencies..."
ADD="$ADDRB" EXCLUDE="$EXRB" requiredbuilder -v -y -s $RELEASEDIR $PKG
echo "Creating package $NAME-$(echo VERSION|rc - _)-$ARCH-$BUILD.txz"

/sbin/makepkg -l y -c n $RELEASEDIR/${PKGNAM}-$(echo $VERSION | tr - _)-$ARCH-$BUILD.txz
fi
